
function openNav() {
    document.querySelector('nav').style.width = "90%"
    document.querySelector('.nav-button').innerHTML = '<i class="fas fa-times-circle"></i>'
}

function closeNav() {
    document.querySelector('nav').style.width = "50px"
    document.querySelector('.nav-button').innerHTML = '<i class="fas fa-arrow-right"></i>'
}

let isNavOpen = false

function openOrCloseNav() {
    if (isNavOpen == true) {
        isNavOpen = false
        closeNav()
    } else {
        isNavOpen = true
        openNav()
    }
}